#this script forms the baseline for a perceptron based part of speech tagger
#following steps are implemented: 
# 1) extract features for a given word in given pos-tagged file
# 2) build a nested dictionary with each tag and its occurring features and weighths (weights are feature counts)
# 3) have the model guess the POS tag for requested word, given the previously counted weights
# 4) tune the weights for an incorrectly guessed POS tag (increase correct and decrease incorrect feature weights)
# 5) repeat 3) and 4) until correct tag is predicted by model


def create_word_list(file):
	"""creates list of words in given file"""
	words_list = []
	with open (file, "rt") as file:
		for line in file:
			if line.split():
				words_list.append(line.split()[0])

	return words_list


def create_tag_list(file):
	"""creates list of tags in given file"""
	tag_list = []
	with open (file, "rt") as file:
		for line in file:
			if line.split():
				tag_list.append(line.split()[1])

	return tag_list


def extract_features(file):
	"""extracts features for all words"""
	words_list = create_word_list(file)
	features = []
	index = 0			
	for word in words_list:
		#extracts features for each word and writes them into list
		feature_list = []
		feature_list.append("%s=%s" %("w", word))

		if index >= 1 and index < len(words_list)-1:
			feature_list.append("%s=%s" %("w-1", words_list[index-1]))
			feature_list.append("%s=%s" %("w+1", words_list[index+1]))
			index += 1

		elif index == 0:
			feature_list.append("%s=%s" %("w+1", words_list[index+1]))
			index += 1

		elif index == len(words_list)-1:
			feature_list.append("%s=%s" %("w-1", words_list[index-1]))
			index += 1
		#appends feature list of each word to one big feature list which will be returned
		features.append(feature_list)

	return features


def features_word(file, word):
	""" searches for requested word in given file and returns a list of its features"""
	features = extract_features(file)
	words = create_word_list(file)

	word_features = []
	indeces = []
	index = 0

	#writes all indeces of requested word in file's file into a list
	for w in words:
		if w == word:
			indeces.append(index)
		index += 1
	
	#accesses indeces to extract only requested word's features
	for index in indeces:
		for feature in features[index]:
			if feature not in word_features:
				word_features.append(feature)

	if indeces:
		return word_features
	else: #if word is not in corpus
		print("Your requested word does not appear in the corpus.")



def initialize_weights(file):
	"""assigns weights (=counts) to each non-zero-feature
	output is a nested dictionary: {tag1: {feature1: weight1}, {feature2: weight2}} etc."""
	tag_list = create_tag_list(file)
	features = extract_features(file)

	weights = {}
	index = 0

	for tag in tag_list:
		#writes all tags into the dictionary (first key)
		if tag not in weights:
			weights[tag] = {}
			feat = features[index]
			#writes features for tag into dictionary (second key + value)
			for f in feat:
				weights[tag][f] = 1
		#if tag already in dictionary, updates weights (= features' counts)
		elif tag in weights:
			feat = features[index]
			for f in feat:
				if f not in weights.get(tag):
					weights[tag][f] = 1
				else:
					weights[tag][f] += 1
		index += 1

	return weights


def perceptron_guess(weights, features):
	"""calculates sum of each tag's weights and returns tag with highest sum as guess"""
	guess = "The model can't predict a tag because the requested word does not appear in the corpus."
	sum_tag = 0
	best_tag = 0

	for tag in weights:
		for feature in weights[tag]:
			#sums up weights for each tag
			try:
				if feature in features:
					weight = weights[tag][feature]
					sum_tag += weight	
			#in case there are no features because word doesn't appear in corpus	
			except TypeError:
				continue
		#if last tag's weights are higher than best tag's weights
		if sum_tag > best_tag:
			best_tag = sum_tag
			guess = tag
		sum_tag = 0

	return guess


def decrease_weights(weights, predicted_tag):
	""""decreases feature weights if predicted tag is incorrect"""
	factor = 0.75 #(add learning rate?)

	for features in weights[predicted_tag]:
		weights[predicted_tag][features] *= factor

	return weights


def increase_weights(weights, correct_tag):
	"""increases feature weights for correct tag if model did not predict it"""
	factor = 1.25 #(add learning rate?)

	for features in weights[correct_tag]:
		weights[correct_tag][features] *= factor

	return weights


def train_pos(file, word, correct_tag):
	"""prints predicted tag, if tag is incorrect calls function to tune weights"""
	features = features_word(file, word)
	weights = initialize_weights(file) #initial weights
	predicted_tag = perceptron_guess(weights, features)
	
	#as long as pediction is incorrect, weights will be tuned
	while predicted_tag != correct_tag:
		print(predicted_tag)
		#decreases incorrect tags' feature weights
		weights = decrease_weights(weights, predicted_tag) 
		#increases correct tag's feature weights (if not predicted)
		weights = increase_weights(weights, correct_tag) 
		#make new prediction with tuned weights
		predicted_tag = perceptron_guess(weights, features) 
		
	print(predicted_tag)
	return weights



def train_pos_file(file):
	"""predicts tags for every word in file and prints them into new file"""
	words = create_word_list(file)
	tags = create_tag_list(file)
	predicted_tags = []
	weights = initialize_weights(file)
	index_file = 0
	index_tag = 0

	for w in words:
		features = features_word(file, w)
		predicted_tag = perceptron_guess(weights, features)
		predicted_tags.append(predicted_tag)
		# correct_tag = tags[index_tag]
		# if prediction is incorrect, tunes weights once and makes new prediction
		if predicted_tag != correct_tag:
			#print(predicted_tag)
			#decreases incorrect tags' feature weights
			weights = decrease_weights(weights, predicted_tag) 
			#increases correct tag's feature weights (if not predicted)
			weights = increase_weights(weights, correct_tag) 
			#make new prediction with tuned weights
			predicted_tag = perceptron_guess(weights, features) 
		index_tag  += 1

	#writes predicted tags into file, along with corresponding words
	#file can be used to calculate Micro and Macro F-Scores
	with open("prediction.txt", "w") as output:
		for tag in predicted_tags:
			output.write(words[index_file] + "	" + tag + "\n")
			index_file += 1


if __name__ == '__main__':
	#running code from shell
    import sys
    file = sys.argv[1]
    #word = sys.argv[2]
    #tag = sys.argv[3]
    train_pos_file(file)
